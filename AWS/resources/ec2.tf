provider "aws" {
    access_key = "XXXXXXXXXXXXXXXXXXXXXXXXXX"
    secret_key = "YYYYYYYYYYYYYYYYYYYYYYYYYY"
    region = "eu-west-2"
}
#resource "aws_instance" "web" {
#  ami           = "ami-0fbec3e0504ee1970"
#  instance_type = "t2.nano"
#}

#resource "aws_instance" "web2" {
#  ami           = "ami-096cb92bb3580c759"
#  instance_type = "t2.micro"
#}

resource "aws_instance" "web" {
  #count = 4
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
 #  instance_type = "t2.nano"
  tags = {
    Name = lower(var.instance_name)
  }
}


data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]

}

resource "aws_eip" "lb" {
  #count = 4
  instance = aws_instance.web.id #[count.index].id
  vpc      = true

}


