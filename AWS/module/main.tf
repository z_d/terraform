module "backend_dev" {
  source = "../resources"
  #instance_type = "t2.micro"
  instance_name = "QWER3"
}

module "backend_test" {
  source = "../resources"
  #instance_type = "t2.micro"
  instance_name = "qwer2" 
}

module "backend_prod" {
  source = "../resources"
  instance_name = "QweR1"
}

#terraform {   #https://www.udemy.com/course/terraform-ru-aws/learn/lecture/19185234#content
#  backend "s3" {         #https://stackoverflow.com/questions/55449909/error-while-configuring-terraform-s3-backend
#    bucket = "bucketestdz"    #https://www.terraform.io/docs/language/settings/backends/s3.html
#    key    = "tfstate/ec2.tfstate"
#    region = "eu-west-2"
#  }
#}
